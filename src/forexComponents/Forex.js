import React, {useState} from 'react';
import ForexApp from '../ForexApp';
import ForexDropdown from './ForexDropdown';
import ForexInput from './ForexInput';
import { Button } from 'reactstrap';
import ForexTable from './ForexTable';

const Forex = () => {
    
    const [amount, setAmount] = useState(0);
    const [baseCurrency, setBaseCurrency] = useState('');
    const [targetCurrency, setTargetCurrency] = useState('');
    const [convertedAmount, setConvertedAmount] = useState(0);
    const [showTable, setShowTable] = useState(false);
    const [rates, setRates] = useState([]);

    // state = {
    //     amount: 0,
    //     baseCurrency: "",
    //     targetCurrency: "",
    //     convertedAmount: 0,
    //     rates: {}
    // }

    const handleAmount = event => {
        // this.setState({
        //     amount: e.target.value
        // });
        setAmount(event.target.value);
    }

    const handleBaseCurrency = currency => {

        const code = currency.code;
        
        
        fetch('https://api.exchangeratesapi.io/latest?base='+code)
            .then( res => res.json())
            .then( res => {
                const ratesArray = Object.entries(res.rates);

                console.log(ratesArray);

                setRates(ratesArray);
                setShowTable(true);
            });


       setBaseCurrency(currency);
       setShowTable(true);

        }
       
            
   
                
       
            

    const handleTargetCurrency = currency => {

        setTargetCurrency(currency);
    }
    

    const handleConvert = () => {

        if(targetCurrency===""|| baseCurrency ===""){
            alert('Please select Currency');
        }else{
            if(amount <=0){
                alert('Invalid amount')
            }else{

            }
            const code = baseCurrency.code; 
        

            fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then( res => res.json())
            .then( res => {
                // console.log(res)
                const targetCode = targetCurrency.code;
    
                const rate = res.rates[targetCode];
    
                // console.log(rate);
                // this.setState({ convertedAmount: this.state.amount * rate })
                setConvertedAmount(amount*rate);
            });
        }
 
    }
        return (
            <div className='bg-info'
                style={{width: "70%"}}    
        
            >
                <h1 className='text-center my-5'>Forex Calculator</h1>
                {
                    showTable === true
                    ?
                    <div>
                        <h2 className='text-center'>Exchange Rate for: {baseCurrency.currency}</h2>
                        <div className='d-flex justify-content-center'>
                            <ForexTable
                                rates = { rates }
                             />
                        </div>
                    </div>
                    : ""
                }
                <div
                    className='d-flex justify-content-around'
                    style={{margin:'0 200px'}}
                >
                    <ForexDropdown
                        label = { 'Base Currency' }
                        onClick = { handleBaseCurrency }
                        currency = { baseCurrency }
                     />
                    
                     <ForexDropdown
                        label = { 'Target Currency' }
                        onClick = { handleTargetCurrency }
                        currency = { targetCurrency }
                     />
                </div>
                <div
                 className='d-flex justify-content-around align-items-center'
                >
                     <ForexInput
                    label = { 'Amount'}
                    placeholder = {'Amount to Convert'}
                    onChange = { handleAmount }
                    />
                    <Button
                        style={{borderRadius: "40px", height: "100%", width:"30%", color: "dark", fontWeight: "700"}}
                        color='success'
                        onClick = { handleConvert }
                    >
                    Convert
                    </Button>
                </div>
                <div>
                    <h1 className='text-center'>{ convertedAmount }{ targetCurrency.code }</h1>
                </div>
                </div>
         )
        
    }


export default Forex;
