import React from 'react';
import Calculator from './calComponent/Calculator';


const CalcApp = () => {
    return (
        <div
        className='bg-primary d-flex justify-content-center min-vh-100'
    >
        <Calculator />
    </div>
    )
}

export default CalcApp;