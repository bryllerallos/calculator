import React, {useState} from 'react';
import FirstInput from './FirstInput';
import SecondInput from './SecondInput';
import Functions from './Functions';
import Result from './Result';


const Calculator = () => {

    const [firstValue, setFirstValue]=useState();
    const [secondValue, setSecondValue]=useState();
    const [symbol, setSymbol]=useState("");
    let [result, setResult]=useState(0);

    const handleFirstValue = event => {
        setFirstValue(event.target.value);
        
    }

    const handleSecondValue = event => {
        setSecondValue(event.target.value);
        
    }

    const handleAdd =  () =>{
            setSymbol('+')
            setResult(result = parseFloat(firstValue) + parseFloat(secondValue))
      
    }

    const handleSubtract =  () =>{
            setSymbol('-')
            setResult(result = parseFloat(firstValue) - parseFloat(secondValue))
    }

    const handleDivide =  () =>{
            setSymbol('/')
            setResult(result = parseFloat(firstValue) / parseFloat(secondValue))
    }
    const handleMultiply =  () =>{
            setSymbol('*')
            setResult(result = parseFloat(firstValue) * parseFloat(secondValue))
    }

    
    const handleClear = () => {
            setFirstValue('')
            setSymbol('')
            setSecondValue('')
            setResult('')
    }

    const handleAddResult = () => {
        setResult(result = parseFloat(firstValue) + parseFloat(secondValue))
        console.log(result)
    }

    return (
        <div className='py-5'>
            <h1 className='text-center'>My Calculator</h1>
            <h2 className='text-right'>{firstValue}{symbol}{secondValue}</h2>
            <FirstInput
                    placeholder = {'First Digit'}
                    onChange = { handleFirstValue }
                    />
             
            <SecondInput
                    placeholder = {'Second Digit'}
                    onChange = { handleSecondValue }
             />

            <Functions
                handleAdd = {handleAdd}
                handleSubtract = {handleSubtract}
                handleMultiply = {handleMultiply}
                handleDivide = {handleDivide}
                handleClear = {handleClear}
            
             />

          <div className="m-5">
                {result ? <h2>Result: {result}</h2> : ""}
         
                 </div>      

        </div>
        
    )
}

export default Calculator;
