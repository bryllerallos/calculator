import React from 'react';
import Button from './Button';

const Functions = (props) => {
    return ( 
        <div className='d-flex flex-row m-2'>
            <Button
                class='btn btn-success m-2 rounded'
                text = {'+'}
                onClick = {props.handleAdd}
            />
            <Button 
                class='btn btn-danger m-2'
                text = {'-'}
                onClick = {props.handleSubtract}
            />
            <Button 
                class='btn btn-info m-2'
                text = {'/'}
                onClick = {props.handleDivide}
            />
            <Button 
                class='btn btn-warning m-2'
                text = {'*'}
                onClick = {props.handleMultiply}
            />
            
            <Button
                class='btn btn-secondary m-2'
                text = {'C'}
                onClick = {props.handleClear}
             />  
             
            </div> 
        
     );
}
 
export default Functions;
