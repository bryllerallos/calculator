import React from 'react';
import {FormGroup, Label, Input} from 'reactstrap';

const SecondInput = (props) => {
    return(
        <FormGroup>
        <Label>{props.label}</Label>
        <Input
            placeholder = { props.placeholder }
            defaultValue = { props.defaultValue }
            onChange = { props.onChange }
            type = 'number'
        />

    </FormGroup>
    )
} 

export default SecondInput;