import React, {Component} from 'react';

class Button extends Component {

    //props of property
    render (){
        return(
            <button
             className={this.props.color}
             //step 4: use the function we received as property
             onClick = {this.props.handleOnclick}
         
             >{this.props.text}</button>
        )
    }
}


export default Button;