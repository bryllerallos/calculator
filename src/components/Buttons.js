import React, {Component} from 'react';
import Button from './Button';

class Buttons extends Component {
    render(){
        return (
            <div 
        className='d-flex justify-content-around'
        style={{
          margin: '0 200px'
        }}
        >
          <Button
            color={'btn-info'}
            text={'+1'}
            //step 3: pass the function we received as a property to button component
            Onclick = {this.props.handleAdd}
           />
          <Button
                color={'btn-warning'}
                text={'-1'}
                handleOnclick = {this.props.handleMinus}
           />
          <Button
                color={'btn-success'}
                text={'reset'}
                handleOnclick = {this.props.handleReset}
           />
          <Button
                color={'btn-secondary'}
                text={'X2'}
                handleOnclick = {this.props.handleMultiply}
           />

           <Button 
                color={'btn-danger'}
                text={'/2'}
                handleOnclick = {this.props.handleDivide}
           />

            

        </div>     
        )
    }
}

export default Buttons;